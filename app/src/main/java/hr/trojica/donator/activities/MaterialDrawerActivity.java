package hr.trojica.donator.activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeader;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.parse.ParseObject;

import hr.trojica.donator.R;
import hr.trojica.donator.classes.Donation;
import hr.trojica.donator.fragments.ParseDonationList;
/**
 * Created by Ivan Kozic on 19.4.2015.
 */
public class MaterialDrawerActivity extends ActionBarActivity
        implements ParseDonationList.OnDonationSelectedListener{

    private static final String CURRENT_DONATION_KEY = "current_donation";

    private ParseObject mAboutDonation;
    //returns Donation selected in listview back to fragments parent activity
    public void onDonationSelected(ParseObject tempObject){
        mAboutDonation = tempObject;
        FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();

        fragTransaction.addToBackStack(null);
        fragTransaction.replace(R.id.mainFrameLayout, Fragment.instantiate(MaterialDrawerActivity.this, "hr.trojica.donator.fragments.AboutDonationFragment"));
        fragTransaction.commit();
    }

    public int mCurrentItemSelected;
    public AccountHeader.Result mDrawerHeader = null;
    public Drawer.Result mDrawerWithItems = null;
    private Toolbar mToolbar;
    private ActionMode mActionMode;
    private Donation mCurrentDonation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_material_drawer);

        mToolbar = (Toolbar)findViewById(R.id.app_toolbar);

        setSupportActionBar(mToolbar);

        /*when app opens, default first selected item will be  -Home*/
        mCurrentItemSelected = 1;

        //sets up the MaterialDrawer basic layout
        mDrawerHeader = new AccountHeader()
                .withActivity(this)
                .withHeaderBackground(R.drawable.donator_drawer_image)
                .withSavedInstance(savedInstanceState)
                .build();
        //sets up the items (buttons, dividers) visible in drawer
        mDrawerWithItems = new Drawer()
                .withActivity(this)
                .withAccountHeader(mDrawerHeader)
                .withToolbar(mToolbar)
                .withHeaderDivider(false)
                .withTranslucentStatusBar(false)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.section_one_home).withIdentifier(1),
                        new PrimaryDrawerItem().withName(R.string.section_two_add_donation).withIdentifier(2),
                        new PrimaryDrawerItem().withName(R.string.section_three_add_need).withIdentifier(3),
                        new DividerDrawerItem(),
                        new PrimaryDrawerItem().withName(R.string.section_four_donation_list).withIdentifier(4),
                        new PrimaryDrawerItem().withName(R.string.section_five_need_list).withIdentifier(5),
                        new DividerDrawerItem(),
                        new PrimaryDrawerItem().withName(R.string.section_six_about).withIdentifier(6)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id, IDrawerItem drawerItem) {
                        if (drawerItem != null) {
                            if (drawerItem.getIdentifier() == 1) {
                                //opens the "HomeFragment" fragment
                                mCurrentItemSelected = 1;

                                FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
                                //custom SlideIn and SlideOut animation when changing fragments
                                fragTransaction.setCustomAnimations(R.anim.slide_in, R.anim.slide_out);

                                fragTransaction.addToBackStack(null);
                                fragTransaction.replace(R.id.mainFrameLayout, Fragment.instantiate(MaterialDrawerActivity.this, "hr.trojica.donator.fragments.HomeFragment"));

                                fragTransaction.commit();
                            } else if (drawerItem.getIdentifier() == 2) {
                                //opens the "AddDonationFragment" fragment
                                mCurrentItemSelected = 2;

                                FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();

                                //rather not use animation on this fragment since it really shows our ugly, hacky toolbar
                                //fragTransaction.setCustomAnimations(R.anim.slide_in, R.anim.slide_out);
                                mCurrentDonation = new Donation();
                                fragTransaction.addToBackStack(null);
                                fragTransaction.replace(R.id.mainFrameLayout, Fragment.instantiate(MaterialDrawerActivity.this, "hr.trojica.donator.fragments.AddDonationFragment"));

                                fragTransaction.commit();
                            } else if (drawerItem.getIdentifier() == 3) {
                                //opens the "AddNeedFragment" fragment
                                mCurrentItemSelected = 3;

                                FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
                                fragTransaction.setCustomAnimations(R.anim.slide_in, R.anim.slide_out);
                                fragTransaction.addToBackStack(null);
                                fragTransaction.replace(R.id.mainFrameLayout, Fragment.instantiate(MaterialDrawerActivity.this, "hr.trojica.donator.fragments.AddNeedFragment"));

                                fragTransaction.commit();
                            } else if (drawerItem.getIdentifier() == 4) {
                                //opens the "ParseDonationList" fragment
                                mCurrentItemSelected = 4;

                                FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
                                fragTransaction.setCustomAnimations(R.anim.slide_in, R.anim.slide_out);
                                fragTransaction.addToBackStack(null);
                                fragTransaction.replace(R.id.mainFrameLayout, Fragment.instantiate(MaterialDrawerActivity.this, "hr.trojica.donator.fragments.ParseDonationList"));

                                fragTransaction.commit();
                            } else if (drawerItem.getIdentifier() == 5) {
                                //opens the "NeedListFragment" fragment
                                mCurrentItemSelected = 5;

                                FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
                                fragTransaction.setCustomAnimations(R.anim.slide_in, R.anim.slide_out);
                                fragTransaction.addToBackStack(null);
                                fragTransaction.replace(R.id.mainFrameLayout, Fragment.instantiate(MaterialDrawerActivity.this, "hr.trojica.donator.fragments.NeedListFragment"));

                                fragTransaction.commit();
                            } else if (drawerItem.getIdentifier() == 6) {
                                mCurrentItemSelected = 6;

                                FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
                                fragTransaction.setCustomAnimations(R.anim.slide_in, R.anim.slide_out);
                                fragTransaction.addToBackStack(null);
                                fragTransaction.replace(R.id.mainFrameLayout, Fragment.instantiate(MaterialDrawerActivity.this, "hr.trojica.donator.fragments.AboutAppFragment"));
                                fragTransaction.commit();
                            }
                        }
                    }

                })
                .withSavedInstance(savedInstanceState)
                .build();

        mDrawerWithItems.getListView().setVerticalScrollBarEnabled(false);

        if(savedInstanceState == null){
            mDrawerWithItems.setSelectionByIdentifier(1);
        }
    }
    @Override
    /**
     * On pressing the "BACK" button
     *  - closes the drawer if it is currently open
     */
    public void onBackPressed() {
        if (mDrawerWithItems != null && mDrawerWithItems.isDrawerOpen()) {
            mDrawerWithItems.closeDrawer();
        }
        else if(mActionMode != null){
            mActionMode.finish();
        }
        else {
            if (getFragmentManager().getBackStackEntryCount() > 0) {
                getFragmentManager().popBackStack();
            } else {
                super.onBackPressed();
            }
        }
    }
    @Override
    protected void onSaveInstanceState(Bundle savedState) {

        savedState = mDrawerWithItems.saveInstanceState(savedState);
        savedState.putSerializable(CURRENT_DONATION_KEY, mCurrentDonation);
        super.onSaveInstanceState(savedState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if(savedInstanceState != null){
            if(savedInstanceState.getSerializable(CURRENT_DONATION_KEY) != null){
                mCurrentDonation = (Donation)savedInstanceState.getSerializable(CURRENT_DONATION_KEY);
        }
        super.onRestoreInstanceState(savedInstanceState);
        }
    }

    /**
     * Gets current donation for "AddDonationFragment"
     * @return mCurrentDonation
     */
    public Donation getmCurrentDonation() {
        return mCurrentDonation;
    }

    /**
     * Gets current donation for "
     * @return mAboutDonation
     */
    public ParseObject getmAboutDonation(){
        return mAboutDonation;
    }

    public Drawer.Result getDrawer(){
        return mDrawerWithItems;
    }
}

package hr.trojica.donator.activities;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseObject;

import hr.trojica.donator.classes.Donation;
import hr.trojica.donator.classes.DonationCategory;
import hr.trojica.donator.classes.Need;
import hr.trojica.donator.R;


public class DonatorApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        //register our objects
        ParseObject.registerSubclass(Need.class);
        ParseObject.registerSubclass(Donation.class);
        ParseObject.registerSubclass(DonationCategory.class);
        Parse.enableLocalDatastore(this);
        //initialize our app with Parse ApplicationID and ClientKey
        Parse.initialize(this, getString(R.string.parse_app_id), getString(R.string.parse_client_key));
    }
}

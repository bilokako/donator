package hr.trojica.donator.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.parse.ParseUser;
import com.parse.ui.ParseLoginBuilder;

/**
 * This is the starting point activity which decides whether to call ParseLogin
 * or, if user is already loggedin, go straight to the main app.
 * (kinda buggy, after login the app crashes, gotta restart it, then works fine)
 */
public class MainActivity extends Activity {
    private static final int LOGIN_REQUEST = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //check if there is a user already logged in
        if (ParseUser.getCurrentUser() == null) {
            //start a login activity
            ParseLoginBuilder loginBuilder = new ParseLoginBuilder(MainActivity.this);
            startActivityForResult(loginBuilder.build(), LOGIN_REQUEST);
        } else {
            //if user already logged in, start the materialDrawer activity (main menu)
            Intent i = new Intent(this, MaterialDrawerActivity.class);
            //once the login has done its job, we do not need this activity in back stack -> exit app on back press
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOGIN_REQUEST) {
            if (resultCode == RESULT_CANCELED) {
                finish();
            } else {
                //if login succeeds, start the materialDrawer activity (main menu)
                Intent i = new Intent(this, MaterialDrawerActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
        }
    }
}

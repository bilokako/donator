package hr.trojica.donator.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import hr.trojica.donator.R;
import hr.trojica.donator.classes.Donation;

public class DonationListAdapter extends ParseQueryAdapter<ParseObject> {

    public DonationListAdapter(Context context) {
        // Use the QueryFactory to construct a PQA that will show donations
        super(context, new ParseQueryAdapter.QueryFactory<ParseObject>() {
            public ParseQuery create() {
                ParseQuery query = new ParseQuery(Donation.class);
                query.orderByDescending("createdAt");
                return query;
            }
        });
    }

    // Customize the layout by overriding getItemView
    @Override
    public View getItemView(ParseObject object, View v, ViewGroup parent) {
        if (v == null) {
            v = View.inflate(getContext(), R.layout.donation_list_item, null);
        }

        super.getItemView(object, v, parent);

        // Add and download the image
        ParseImageView mTodoImage = (ParseImageView) v.findViewById(R.id.icon);
        ParseFile imageFile = object.getParseFile("photo");
        if (imageFile != null) {
            mTodoImage.setParseFile(imageFile);
            mTodoImage.loadInBackground();
        }

        // Add the title view
        TextView titleTextView = (TextView) v.findViewById(R.id.text1);
        titleTextView.setText(object.getString("name"));

        // Add a reminder of how long this item has been outstanding
        TextView timestampView = (TextView) v.findViewById(R.id.description);
        timestampView.setText(object.getString("description"));
        return v;
    }

}

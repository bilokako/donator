package hr.trojica.donator.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import hr.trojica.donator.R;
import hr.trojica.donator.classes.Need;

/**
 * Created by Dule on 25.4.2015..
 */
public class NeedListAdapter extends ParseQueryAdapter<Need> {

    public NeedListAdapter(Context context) {
        super(context, new ParseQueryAdapter.QueryFactory<Need>(){

            @Override
            public ParseQuery<Need> create() {
                ParseQuery<Need> query = new ParseQuery<>(Need.class);
                query.orderByDescending("createdAt");
                return query;
            }
        });
    }

    @Override
    public View getItemView(Need object, View v, ViewGroup parent) {
        if(v == null){
            v = View.inflate(getContext(), R.layout.need_list_item, null);
        }
        super.getItemView(object, v, parent);

        TextView titleView = (TextView)v.findViewById(R.id.need_list_item_title);
        TextView descriptionView = (TextView)v.findViewById(R.id.need_list_item_description);

        titleView.setText(object.getName());
        descriptionView.setText(object.getDescription());

        return v;
    }

    //Parse should have done this
    @Override
    public void notifyDataSetChanged() {
        loadObjects();
        super.notifyDataSetChanged();
    }
}

package hr.trojica.donator.adapters;

import android.app.Activity;
import android.content.Context;

import com.parse.ParseObject;
import com.parse.ParseQueryAdapter;

/**
 * Created by Dule on 14.4.2015..
 */
public class ParseSpinnerAdapter<T extends ParseObject> extends ParseQueryAdapter<T> {

    public ParseSpinnerAdapter(Context context, Class clazz) {
        super(context, clazz);
    }

    public ParseSpinnerAdapter(Activity activity, Class<T> donationCategoryClass, int resId) {
        super(activity, donationCategoryClass, resId);
    }


    @Override
    public int getViewTypeCount() {
        return 1;
    }
}

package hr.trojica.donator.classes;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by Dule on 24.4.2015..
 */

@ParseClassName("Need")
public class Need extends ParseObject{

    public String getName(){
        return getString("name");
    }

    public void setName(String name){
        put("name", name);
    }

    public String getDescription() {
        return getString("description");
    }

    public void setDescription(String description) {
        put("description", description);
    }

    public DonationCategory getCategory() {
        return (DonationCategory) get("category");
    }

    public void setCategory(DonationCategory category) {
        put("category", category);
    }

}

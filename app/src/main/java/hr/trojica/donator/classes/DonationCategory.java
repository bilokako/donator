package hr.trojica.donator.classes;

import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("DonationCategory")
public class DonationCategory extends ParseObject {

    public String getName() {
        return getString("name");
    }

    public void setName(String name) {
        put("name", name);
    }

}

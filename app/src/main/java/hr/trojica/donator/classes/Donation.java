package hr.trojica.donator.classes;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.io.Serializable;


@ParseClassName("Donation")
public class Donation extends ParseObject implements Serializable{

    public Donation(){

    }

    public String getName(){
        return getString("name");
    }

    public void setName(String name){
        put("name", name);
    }

    public String getDescription() {
        return getString("description");
    }

    public void setDescription(String description) {
        put("description", description);
    }

    public ParseUser getOwner(){
        return getParseUser("owner");
    }

    public void setOwner(ParseUser user){
        put("owner", user);
    }

    public ParseFile getPhoto(){
        return getParseFile("photo");
    }

    public void setPhoto(ParseFile photo){
        put("photo", photo);
    }

    public DonationCategory getCategory() {
        return (DonationCategory) get("category");
    }

    public void setCategory(DonationCategory category) {
        put("category", category);
    }

    
}

package hr.trojica.donator.fragments;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.OrientationEventListener;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.SaveCallback;
import com.shamanland.fab.FloatingActionButton;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import hr.trojica.donator.R;
import hr.trojica.donator.activities.MaterialDrawerActivity;

public class CameraFragment extends Fragment {

    public static final String TAG = "CameraFragment";

    private Camera mCamera;
    private SurfaceView mSurfaceView;
    private ParseFile mPhotoFile;
    private FloatingActionButton mPhotoButton;
    private ProgressDialog mProgress;

    private OrientationEventListener mOrientationListener;
    private int mRotation;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.camera, parent, false);

        mProgress = new ProgressDialog(v.getContext());
        mPhotoButton = (FloatingActionButton) v.findViewById(R.id.camera_photo_button);


        // FAB moves only on entering camera
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSharedElementReturnTransition(null);
        }

        // Checks orientation for image rotate
        mOrientationListener = new OrientationEventListener(getActivity()) {
            @Override
            public void onOrientationChanged(int or) {
                if(or >= 330 || or <= 30) mRotation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                if(or >= 240 && or <= 300 ) mRotation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                if(or >= 60 && or <= 120) mRotation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
            }
        };

        if(mOrientationListener.canDetectOrientation()){
            mOrientationListener.enable();
        }

        // Orientation will be locked, it's checked above
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        if (mCamera == null) {
            try {
                mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
                mPhotoButton.setEnabled(true);
            } catch (Exception e) {
                Log.e(TAG, "No camera with exception: " + e.getMessage());
                mPhotoButton.setEnabled(false);
                Toast.makeText(getActivity(), "No camera detected",
                        Toast.LENGTH_LONG).show();
            }
        }

        mPhotoButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mCamera == null)
                    return;

                mOrientationListener.disable();

                /* autoFocus() works even if there's no auto focus hardware feature
                   http://developer.android.com/reference/android/hardware/Camera.html#autoFocus%28android.hardware.Camera.AutoFocusCallback%29
                 */
                mCamera.autoFocus(new Camera.AutoFocusCallback() {
                    @Override
                    public void onAutoFocus(boolean success, Camera camera) {
                        camera.takePicture(new Camera.ShutterCallback() {

                            @Override
                            public void onShutter() {
                                // nothing to do
                            }

                        }, null, new Camera.PictureCallback() {

                            @Override
                            public void onPictureTaken(byte[] data, Camera camera) {
                                mProgress.setMessage(getString(R.string.save_picture_progress));
                                mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                mProgress.show();

                                saveScaledPhoto(data);
                            }

                        });
                    }
                });


            }
        });

        mSurfaceView = (SurfaceView) v.findViewById(R.id.camera_surface_view);


        SurfaceHolder holder = mSurfaceView.getHolder();

        holder.addCallback(new Callback() {

            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    if (mCamera != null) {
                        mCamera.setPreviewDisplay(holder);

                    }
                } catch (IOException e) {
                    Log.e(TAG, "Error setting up preview", e);
                }
            }

            public void surfaceChanged(SurfaceHolder holder, int format,
                                       int width, int height) {
                mCamera.setDisplayOrientation(90);
                Camera.Parameters params = mCamera.getParameters();
                Camera.Size bestSize = getBestPreviewSize(height, width);
                params.setPreviewSize(bestSize.width, bestSize.height);
                mCamera.setParameters(params);
                mCamera.startPreview();

            }

            public void surfaceDestroyed(SurfaceHolder holder) {
                // nothing here
            }

        });

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        // Set hide ActionBar
        ((MaterialDrawerActivity) getActivity()).getSupportActionBar().hide();

        super.onViewCreated(view, savedInstanceState);
    }

    private void saveScaledPhoto(byte[] data) {

        // Resize photo from camera byte array
        Bitmap image = BitmapFactory.decodeByteArray(data, 0, data.length);
        Bitmap imageScaled = Bitmap.createScaledBitmap(image, 500, 500
                * image.getHeight() / image.getWidth(), false);

        Matrix matrix = new Matrix();

        // check for camera orientation when the picture was taken
        switch (mRotation){
            case ActivityInfo.SCREEN_ORIENTATION_PORTRAIT:
                matrix.postRotate(90);
                break;
            case ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT:
                matrix.postRotate(-90);
                break;
            case ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE:
                matrix.postRotate(180);
        }

        Bitmap rotatedScaledImage = Bitmap.createBitmap(imageScaled, 0,
                0, imageScaled.getWidth(), imageScaled.getHeight(),
                matrix, true);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        rotatedScaledImage.compress(Bitmap.CompressFormat.JPEG, 100, bos);

        byte[] scaledData = bos.toByteArray();

        // Save the scaled image to Parse
        mPhotoFile = new ParseFile("photo.jpg", scaledData);
        mPhotoFile.saveInBackground(new SaveCallback() {

            public void done(ParseException e) {
                if (e != null) {
                    Toast.makeText(getActivity(),
                            "Error saving: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                } else {
                    addPhotoToDonationAndReturn(mPhotoFile);
                }
                mProgress.hide();
            }
        });
    }

    // Primitive preview size checker
    private Camera.Size getBestPreviewSize(int width, int height)
    {
        Camera.Size result=null;
        Camera.Parameters p = mCamera.getParameters();
        for (Camera.Size size : p.getSupportedPreviewSizes()) {
            if (size.width<=width && size.height<=height) {
                if (result==null) {
                    result=size;
                } else {
                    int resultArea=result.width*result.height;
                    int newArea=size.width*size.height;

                    if (newArea>resultArea) {
                        result=size;
                    }
                }
            }
        }
        return result;
    }

    // Sets photo to currentDonation
    // returns to addDonation which gets photo in onResume()
    private void addPhotoToDonationAndReturn(ParseFile photoFile) {
        ((MaterialDrawerActivity) getActivity()).getmCurrentDonation().setPhoto(
                photoFile);
        FragmentManager fm = getActivity().getFragmentManager();
        fm.popBackStack("AddDonationFragment",
                FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }


    @Override
    public void onResume() {
        super.onResume();
        if (mCamera == null) {
            try {
                mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
                mPhotoButton.setEnabled(true);
            } catch (Exception e) {
                Log.i(TAG, "No camera: " + e.getMessage());
                mPhotoButton.setEnabled(false);
                Toast.makeText(getActivity(), "No camera detected",
                        Toast.LENGTH_LONG).show();
            }
        }
        if(mOrientationListener.canDetectOrientation()){
            mOrientationListener.enable();
        }
    }

    // Clean everything up
    @Override
    public void onPause() {
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        mOrientationListener.disable();

        super.onPause();
    }

    @Override
    public void onDestroyView() {

        // Disable fullscreen
        ((MaterialDrawerActivity) getActivity()).getSupportActionBar().show();

        super.onDestroyView();
    }
}

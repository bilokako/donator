package hr.trojica.donator.fragments;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.SaveCallback;

import hr.trojica.donator.R;
import hr.trojica.donator.activities.MaterialDrawerActivity;
import hr.trojica.donator.classes.Need;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddNeedFragment extends Fragment {

    private EditText mNeedName;
    private EditText mNeedDescription;
    private Button mAddNeedButton;
    private ProgressDialog mProgress;

    private Need mNeed;


    public AddNeedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_add_need, container, false);

        mNeedDescription = (EditText)v.findViewById(R.id.add_need_description);
        mNeedName = (EditText)v.findViewById(R.id.add_need_name);
        mAddNeedButton = (Button)v.findViewById(R.id.add_need_add_button);

        mProgress = new ProgressDialog(v.getContext());

        mNeed = new Need();

        mAddNeedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                mNeed.setName(mNeedName.getText().toString());
                mNeed.setDescription(mNeedDescription.getText().toString());

                mProgress.setMessage(getString(R.string.save_need_progress));
                mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                mProgress.show();

                mNeed.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            mProgress.dismiss();
                            Toast.makeText(v.getContext(),
                                    "Need saved!", Toast.LENGTH_LONG).show();
                        } else {
                            mProgress.dismiss();
                            Toast.makeText(v.getContext(),
                                    "Error saving need: " + e, Toast.LENGTH_LONG).show();
                        }
                        //Back stack will always have at least one fragment.
                        getFragmentManager().popBackStack();
                    }
                });
            }
        });

        ((MaterialDrawerActivity) getActivity()).getSupportActionBar()
                .setTitle(getResources().getString(R.string.add_need_title));

        return v;
    }


}

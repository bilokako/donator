package hr.trojica.donator.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import hr.trojica.donator.R;
import hr.trojica.donator.activities.MaterialDrawerActivity;

/**
 * Created by Ivan Kozic on 30.4.2015..
 */
public class HomeFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState){
        View root = inflater.inflate(R.layout.home_fragment, parent, false);

        //set the toolbars title to match this currently opened fragment
        ((MaterialDrawerActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.home_title));

        return root;
    }
}

package hr.trojica.donator.fragments;


import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.nineoldandroids.view.ViewHelper;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.shamanland.fab.FloatingActionButton;

import hr.trojica.donator.R;
import hr.trojica.donator.activities.MaterialDrawerActivity;
import hr.trojica.donator.adapters.ParseSpinnerAdapter;
import hr.trojica.donator.classes.Donation;
import hr.trojica.donator.classes.DonationCategory;


public class AddDonationFragment extends Fragment {


    private EditText mDonationNameText;
    private Spinner mDonationCategorySpinner;
    private Button mDonationAddButton;
    private FloatingActionButton mDonationCameraButton;
    private ParseImageView mImagePreviewImageView;
    private EditText mDonationDescriptionText;
    private ProgressDialog mProgress;
    private ImageButton mHomeButton;
    private LinearLayout mToolbar;
    private ObservableScrollView mScrollView;
    private TextView mTitle;
    private Donation mDonation;

    public AddDonationFragment() {
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.add_donation, container, false);

        //Hide action bar because this fragment has it's own (faux) toolbar
        ((MaterialDrawerActivity) getActivity()).getSupportActionBar().hide();


        mDonationNameText = (EditText)view.findViewById(R.id.fragment_add_donation_name);
        mDonationCategorySpinner = (Spinner) view.findViewById(R.id.fragment_add_donation_category);
        mDonationAddButton = (Button)view.findViewById(R.id.fragmet_add_donation_add);
        mImagePreviewImageView = (ParseImageView) view.findViewById(R.id.fragment_add_donation_image_preview);
        mDonationDescriptionText = (EditText) view.findViewById(R.id.fragment_add_donation_description);
        mDonationCameraButton = (FloatingActionButton) view.findViewById(R.id.fragment_add_donation_camera);
        mHomeButton = (ImageButton) view.findViewById(R.id.add_donation_home_button);
        mToolbar = (LinearLayout) view.findViewById(R.id.add_donation_toolbar);
        mScrollView = (ObservableScrollView) view.findViewById(R.id.add_donation_scrollview);
        mTitle = (TextView) view.findViewById(R.id.add_donation_title);


        mProgress = new ProgressDialog(view.getContext());

        mHomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // fragments toolbar button opens drawer
                ((MaterialDrawerActivity) getActivity()).getDrawer().openDrawer();
            }
        });

        // Cool scroll effects
        mScrollView.setScrollViewCallbacks(new ObservableScrollViewCallbacks() {
            @Override
            public void onScrollChanged(int i, boolean b, boolean b1) {
                // Hides toolbar if it goes below image
                if (i >= mImagePreviewImageView.getHeight() - mToolbar.getHeight()) {
                    ViewHelper.setTranslationY(mToolbar, -(i - mImagePreviewImageView.getHeight() + mToolbar.getHeight()));
                } else {
                    ViewHelper.setTranslationY(mToolbar, 0);
                }

                //parallax effect on image
                ViewHelper.setTranslationY(mImagePreviewImageView, i / 2);
            }

            @Override
            public void onDownMotionEvent() {

            }

            @Override
            public void onUpOrCancelMotionEvent(ScrollState scrollState) {

            }
        });

        ParseSpinnerAdapter<DonationCategory> parseCategoryAdapter =
                new ParseSpinnerAdapter<>(getActivity(), DonationCategory.class, R.layout.spinner_dropdown_item);

        parseCategoryAdapter.setTextKey("name");

        mDonationCategorySpinner.setAdapter(parseCategoryAdapter);

        mDonationCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mDonation.setCategory((DonationCategory) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //floating action button zooms in on load
        Animation zoomAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom_in);
        mDonationCameraButton.startAnimation(zoomAnim);

        mDonationCameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Hide keyboard on camera fragment show
                InputMethodManager imm = (InputMethodManager) getActivity()
                        .getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

                //Lollipop camera fragment transition
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                    TransitionInflater inflater = TransitionInflater.from(getActivity());

                    //No transition except for FAB motion
                    setExitTransition(inflater.inflateTransition(android.R.transition.no_transition));

                    CameraFragment cf = new CameraFragment();
                    cf.setSharedElementEnterTransition(inflater
                            .inflateTransition(R.transition.camera_button_transition));
                    cf.setEnterTransition(inflater.inflateTransition(android.R.transition.no_transition));

                    //Fragment transition + FAB motion
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.replace(R.id.mainFrameLayout, cf)
                            .addToBackStack("AddDonationFragment")
                            .addSharedElement(mDonationCameraButton, "camera_transition")
                            .commit();

                } else {
                    FragmentTransaction ft = getActivity().getFragmentManager().beginTransaction();
                    CameraFragment cf = new CameraFragment();

                    ft.replace(R.id.mainFrameLayout, cf)
                            .addToBackStack("AddDonationFragment")
                            .commit();
                }

            }
        });

        //Adds donation to Parse
        mDonationAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseUser user = ParseUser.getCurrentUser();


                mDonation.setName(mDonationNameText.getText().toString());
                mDonation.setDescription(mDonationDescriptionText.getText().toString());
                mDonation.setOwner(user);

                mProgress.setMessage(getString(R.string.save_donation_progress));
                mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                mProgress.show();

                mDonation.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            mProgress.dismiss();
                            Toast.makeText(view.getContext(),
                                    "Donation saved!", Toast.LENGTH_LONG).show();
                        } else {
                            mProgress.dismiss();
                            Toast.makeText(view.getContext(),
                                    "Error saving donation: " + e, Toast.LENGTH_LONG).show();
                        }
                        //Back stack will always have at least one fragment.
                        getFragmentManager().popBackStack();
                    }
                });
            }
        });


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Donation resides in activity
        mDonation = ((MaterialDrawerActivity) getActivity()).getmCurrentDonation();

        //Camera sets up photo via parse
        ParseFile photoFile = mDonation.getPhoto();
        if(photoFile != null){
            mImagePreviewImageView.setParseFile(photoFile);
            mImagePreviewImageView.loadInBackground();
        }
    }

    @Override
    public void onDestroyView() {
        // Show action bar for other fragments
        ((MaterialDrawerActivity) getActivity()).getSupportActionBar().show();
        super.onDestroyView();
    }

}

package hr.trojica.donator.fragments;

/**
 * Created by Ivan Kozic on 28.4.2015..
 */
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import hr.trojica.donator.R;
import hr.trojica.donator.activities.MaterialDrawerActivity;
import hr.trojica.donator.helpers.RoundImage;

public class AboutAppFragment extends Fragment {
    ImageView mDev;
    ImageView mDev2;
    ImageView mDev3;
    RoundImage rImage;
    RoundImage rImage2;
    RoundImage rImage3;
    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.about_app_fragment, null);

        context = getActivity();

        ((MaterialDrawerActivity) getActivity()).getSupportActionBar()
                .setTitle(getResources().getString(R.string.about_app_title));

        //draws a circular image in ImageView
        mDev = (ImageView)root.findViewById(R.id.dev_image);
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.hrvoje);
        rImage = new RoundImage(bm);
        mDev.setImageDrawable(rImage);

        mDev2 = (ImageView)root.findViewById(R.id.dev_image2);
        Bitmap bm2 = BitmapFactory.decodeResource(getResources(),R.drawable.dule);
        rImage2 = new RoundImage(bm2);
        mDev2.setImageDrawable(rImage2);

        mDev3 = (ImageView)root.findViewById(R.id.dev_image3);
        Bitmap bm3 = BitmapFactory.decodeResource(getResources(),R.drawable.ivan);
        rImage3 = new RoundImage(bm3);
        mDev3.setImageDrawable(rImage3);

        TextView mFablib = (TextView) root.findViewById(R.id.libtwo_content);
        mFablib.setText(Html.fromHtml(getString(R.string.fab_desc)));

        TextView mScrolllib = (TextView) root.findViewById(R.id.libthree_content);
        mScrolllib.setText(Html.fromHtml(getString(R.string.scroll_desc)));

        TextView mMaterialdrawerlib = (TextView) root.findViewById(R.id.libfour_content);
        mMaterialdrawerlib.setText(Html.fromHtml(getString(R.string.materialdrawer_desc)));

        TextView mParselib = (TextView) root.findViewById(R.id.libfive_content);
        mParselib.setText(Html.fromHtml(getString(R.string.parse_desc)));

        TextView mMaterialEditTextLib = (TextView) root.findViewById(R.id.libsix_content);
        mMaterialEditTextLib.setText(Html.fromHtml(getString(R.string.material_edit_text_desc)));

        //card onClickListeners open MAIL if clicked on developers and library WEBSITE if clicked on implemented libraries
        CardView libTwoCard = (CardView) root.findViewById(R.id.libtwocard);
        libTwoCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent libTwoWeb = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.fab_web)));
                startActivity(libTwoWeb);
            }
        });

        CardView libThreeCard = (CardView) root.findViewById(R.id.libthreecard);
        libThreeCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent libThreeWeb = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.scroll_web)));
                startActivity(libThreeWeb);
            }
        });

        CardView libFourCard = (CardView) root.findViewById(R.id.libfourcard);
        libFourCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent libFourWeb = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.materialdrawer_web)));
                startActivity(libFourWeb);
            }
        });

        CardView libFiveCard = (CardView) root.findViewById(R.id.libfivecard);
        libFiveCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent libFiveWeb = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.parse_web)));
                startActivity(libFiveWeb);
            }
        });

        CardView libSixCard = (CardView) root.findViewById(R.id.libsixcard);
        libSixCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent libSixWeb = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.material_edit_text_web)));
                startActivity(libSixWeb);
            }
        });

        CardView dev = (CardView) root.findViewById(R.id.dev_card);
        dev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dev = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.mail_to) + getResources().getString(R.string.dev_mail)));

                context.startActivity(Intent.createChooser(dev, getResources().getString(R.string.send_mail_using)));
            }
        });

        CardView dev2 = (CardView) root.findViewById(R.id.dev_card2);
        dev2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dev2 = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.mail_to) + getResources().getString(R.string.dev2_mail)));

                context.startActivity(Intent.createChooser(dev2, getResources().getString(R.string.send_mail_using)));
            }
        });

        CardView dev3 = (CardView) root.findViewById(R.id.dev_card3);
        dev3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dev3 = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.mail_to) + getResources().getString(R.string.dev3_mail)));
                //Intent dev3 = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("mailto:" + getResources().getString(R.string.dev3_mail)));

                context.startActivity(Intent.createChooser(dev3, getResources().getString(R.string.send_mail_using)));
            }
        });

        return root;
    }
}

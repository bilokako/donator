package hr.trojica.donator.fragments;

import android.app.AlertDialog;
import android.app.ListFragment;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.view.ActionMode;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.DeleteCallback;
import com.parse.ParseException;

import hr.trojica.donator.R;
import hr.trojica.donator.activities.MaterialDrawerActivity;
import hr.trojica.donator.adapters.NeedListAdapter;
import hr.trojica.donator.classes.Need;


public class NeedListFragment extends ListFragment {

    private int mSelectedItemPosition;
    private ActionMode mActionMode;


    public NeedListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setListAdapter(new NeedListAdapter(getActivity()));
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
    }




    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getListView().setBackgroundColor(getResources().getColor(R.color.background_material_light));

        ((MaterialDrawerActivity) getActivity()).getSupportActionBar()
                .setTitle(getResources().getString(R.string.need_list_title));
    }

    @Override
    public void onDestroyView() {
        ((MaterialDrawerActivity)getActivity()).getSupportActionBar().show();
        if(mActionMode != null){
            mActionMode.finish();
        }
        super.onDestroyView();
    }

    /**
     * Deletes currently selected need.
     * @param position
     */
    private void deleteCurrentNeed(final int position) {
        AlertDialog.Builder alertDialogBuilder =
                new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.need_list_delete_question)
                        .setMessage(R.string.need_list_delete_message)
                        .setPositiveButton(
                                getResources().getString(R.string.need_list_delete_question_delete),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Need toDelete = (Need) getListAdapter().getItem(position);
                                        toDelete.deleteInBackground(new DeleteCallback() {
                                            @Override
                                            public void done(ParseException e) {
                                                ((NeedListAdapter) getListAdapter()).notifyDataSetChanged();
                                            }
                                        });
                                    }
                                })
                        .setNegativeButton(getResources().getString(R.string.need_list_delete_question_keep),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //Do nothing
                                    }
                                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
        brandAlertDialog(alertDialog);
       // alertDialogBuilder.show();
    }

    /**
     * change AlertDialog title and divider color, color is
     * @param dialog
     */
    public static void brandAlertDialog(AlertDialog dialog) {
        try {
            Resources resources = dialog.getContext().getResources();
            int color = resources.getColor(R.color.primary); // set color here

            int alertTitleId = resources.getIdentifier("alertTitle", "id", "android");
            TextView alertTitle = (TextView) dialog.getWindow().getDecorView().findViewById(alertTitleId);
            alertTitle.setTextColor(color); // change title text color

            int titleDividerId = resources.getIdentifier("titleDivider", "id", "android");
            View titleDivider = dialog.getWindow().getDecorView().findViewById(titleDividerId);
            titleDivider.setBackgroundColor(color); // change divider color
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

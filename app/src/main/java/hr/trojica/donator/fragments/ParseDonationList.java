package hr.trojica.donator.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.parse.ParseObject;
import com.parse.ParseQueryAdapter;

import hr.trojica.donator.R;
import hr.trojica.donator.activities.MaterialDrawerActivity;
import hr.trojica.donator.adapters.DonationListAdapter;
import hr.trojica.donator.classes.Donation;

/**
 * Created by Hrvoje on 20.4.2015..
 */
public class ParseDonationList extends Fragment {
    OnDonationSelectedListener mCallback;

    /**
     * Interface used to send the Donation that you select in ListView back to this fragments parent activity.
     * Thank you Google.
     */
    public interface OnDonationSelectedListener {
        void onDonationSelected(ParseObject tempObject);
    }

    private static final String TAG = "DonationListOnItemClick";
    private DonationListAdapter mDonationAdapter;
    private ParseQueryAdapter<Donation> mainAdapter;

    private ListView listView;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (OnDonationSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnDonationSelectedListener");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                             Bundle savedInstanceState){

        View v = inflater.inflate(R.layout.donation_list, parent, false);

        listView=(ListView)v.findViewById(R.id.donation_list_listview);
        //TODO delete this comment
        /*
        mainAdapter = new ParseQueryAdapter<Donation>(getActivity(), Donation.class);
        mainAdapter.setTextKey("name");
        mainAdapter.setImageKey("photo");
        mainAdapter.setTextKey("description");*/

        mDonationAdapter = new DonationListAdapter(getActivity());

        // Set the ListActivity's adapter to be the PQA
        listView.setAdapter(mDonationAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id)
            {
                //TODO delete these Logs
                Log.d(TAG, "Clicked on item number " + position);
                ParseObject tempDonation = mDonationAdapter.getItem(position);
                Log.d(TAG, "Fetched item from adapter, name: " + tempDonation.getString("name"));
                mCallback.onDonationSelected(tempDonation);
            }
        });

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        ((MaterialDrawerActivity) getActivity()).getSupportActionBar()
                .setTitle(getResources().getString(R.string.donation_list_title));
        super.onViewCreated(view, savedInstanceState);
    }
}

package hr.trojica.donator.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.nineoldandroids.view.ViewHelper;
import com.parse.ParseException;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseUser;

import hr.trojica.donator.R;
import hr.trojica.donator.activities.MaterialDrawerActivity;

/**
 * Created by Hrvoje on 24.4.2015...
 *
 * Added AsyncTask and info fetch, Ivan Kozic 27.4.2015.
 */
public class AboutDonationFragment extends Fragment {

    private ParseImageView mPhoto;
    private TextView mDonationNameText;
    private TextView mDonationDescriptionText;
    private TextView mDonationCategory;
    private TextView mDonationPhoneNumber;
    private LinearLayout mTopLinearLayout;
    private ObservableScrollView mScrollView;
    private Button mCallOwner;

    private ParseUser mDonationOwner;
    private ParseObject mAboutDonation;
    private ParseObject mCategory;

    private ProgressBar mProgress;
    public AboutDonationFragment(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAboutDonation = ((MaterialDrawerActivity) getActivity()).getmAboutDonation();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.about_donation, container, false);
        mProgress = (ProgressBar)view.findViewById(R.id.progress_bar_about);
        mScrollView = (ObservableScrollView)view.findViewById(R.id.about_donation_scroll);

        /**
         * All these elements with View.GONE are hidden until donation is loaded from Parse.
         */
        mTopLinearLayout = (LinearLayout)view.findViewById(R.id.about_donation_top_linear_layout);
        mTopLinearLayout.setVisibility(View.GONE);

        mPhoto=(ParseImageView)view.findViewById(R.id.fragment_about_donation_image);
        mPhoto.setVisibility(View.GONE);

        mDonationNameText =(TextView)view.findViewById(R.id.fragment_about_donation_name);
        mDonationDescriptionText =(TextView)view.findViewById(R.id.fragment_about_donation_description);
        mDonationCategory =(TextView)view.findViewById(R.id.fragment_about_donation_category);
        mDonationPhoneNumber =(TextView)view.findViewById(R.id.fragment_about_donation_phone);
        mCallOwner =(Button)view.findViewById(R.id.fragment_about_donation_call);

        //onClick dials owners phone number
        mCallOwner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:"+mDonationPhoneNumber.getText().toString()));
                startActivity(callIntent);
            }
        });

        mPhoto.setParseFile(mAboutDonation.getParseFile("photo"));
        mPhoto.loadInBackground();
        mDonationNameText.setText(mAboutDonation.getString("name"));
        mDonationDescriptionText.setText(mAboutDonation.getString("description"));

        /**
         * Here we execute a task which fetches the whole donation.
         */
        new GrabDonationInfotask().execute();

        mScrollView.scrollTo(0, mScrollView.getBottom());

        mScrollView.setScrollViewCallbacks(new ObservableScrollViewCallbacks() {
            @Override
            public void onScrollChanged(int i, boolean b, boolean b1) {
                ViewHelper.setTranslationY(mPhoto, i / 2);
            }

            @Override
            public void onDownMotionEvent() {
            //this empty
            }

            @Override
            public void onUpOrCancelMotionEvent(ScrollState scrollState) {
            //this also empty
            }
        });

        return view;
    }
    /**
     * Gets info about donation, its category and its owner in another thread.
     */
    private class GrabDonationInfotask extends AsyncTask<String, Void, String>{
        @Override
        protected String doInBackground(String... urls){
            mDonationOwner = mAboutDonation.getParseUser("owner");
            try{
                //get owners info from cloud
                mDonationOwner.fetch();
            } catch (ParseException e){
                //thrown if the server is inaccessible
                e.printStackTrace();
            }
            // get this donations category
            mCategory = mAboutDonation.getParseObject("category");
            try{
                //get category info from cloud
                mCategory.fetch();
            } catch (ParseException e){
                //thrown if the server is inaccessible
                e.printStackTrace();
            }
            mDonationCategory.setText(mCategory.getString("name"));
            mDonationPhoneNumber.setText(mDonationOwner.getString("phone"));

            return "";
        }
        // after task finishes, sets photo, linearylayout to VISIBLE and hides progressbar with GONE
        @Override
        protected void onPostExecute(String result){
            if(mTopLinearLayout != null){
                mTopLinearLayout.setVisibility(View.VISIBLE);
            }
            if(mProgress != null){
                mProgress.setVisibility(View.GONE);
            }
            if(mPhoto != null){
                mPhoto.setVisibility(View.VISIBLE);
            }
            scrollDown(mScrollView);
        }
    }

    private static void scrollDown(final ObservableScrollView scrollView){
        final int bottom = scrollView.getChildAt(0).getHeight();

        new CountDownTimer(600, 5) {

            public void onTick(long millisUntilFinished) {
                float t = 600 - millisUntilFinished;
                float d = 600;
                float factor = ease(t, 0, 1, d);
                scrollView.scrollTo(0, (int)(bottom*factor));
                Log.d("hr.trojica.donator", "to= " + (bottom*factor));
            }

            public void onFinish() {
            }

        }.start();
    }

    private static float ease (float t,float b , float c, float d) {
        return c * (float)Math.sin(t/d * (Math.PI/2)) + b;
    }


}
